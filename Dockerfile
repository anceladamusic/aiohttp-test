FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /aiohttp-test
WORKDIR /aiohttp-test
ADD requirements.txt /aiohttp-test/
RUN pip install -r requirements.txt
ADD . /aiohttp-test/
# RUN apt-get update && apt-get install -y vim

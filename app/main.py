from pathlib import Path

from aiohttp import web

from .settings import Settings
from .views import (
    handle_get,
    handle_post,
)


THIS_DIR = Path(__file__).parent
BASE_DIR = THIS_DIR.parent


def setup_routes(app):
    """ adds view to url """
    app.router.add_routes([
        web.get('/', handle_get),
        web.post('/', handle_post),
    ])


async def create_app():
    """ runs app """
    app = web.Application()
    settings = Settings()
    app.update(
        name='project',
        settings=settings
    )

    setup_routes(app)
    return app

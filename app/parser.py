""" parses packet """

from aiohttp.web import HTTPBadRequest
from schema import Schema, Use, SchemaError


class Parser():
    """ parses and calculates result of packet """

    def __init__(self, **kwargs):
        self.request = kwargs['request']

    async def __check_schema_and_get_packet(self):

        try:
            packet = await self.request.json()
        except Exception as e:
            raise HTTPBadRequest(
                text='No request body', content_type='application/json')

        schema = Schema({
            'station_config': Use(dict),
            'data': Use(dict),
        })

        error = None

        try:
            schema.validate(packet)
        except SchemaError as e:
            error = e.__dict__['autos']

        if error:
            raise HTTPBadRequest(
                text=error[0], content_type='application/json')

        return packet

    @staticmethod
    async def __check_positive(temperature):
        if temperature >= 0:
            return True
        return False

    async def comes_over_zero(self):
        """ calculates result of packet """

        packet = await self.__check_schema_and_get_packet()

        positive = False

        for key, value in packet['data'].items():
            if value['t_air'] >= 0:
                positive = True
            break

        for key, value in packet['data'].items():
            if await self.__check_positive(value['t_air']) != positive:
                return True
        return False

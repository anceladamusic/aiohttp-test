""" handles requests to urls """

import json
from aiohttp import web

from pprint import pprint

from .parser import Parser


async def handle_get(request):
    """ handles get """

    if request.body_exists:
        result = await Parser(**{'request': request}).comes_over_zero()

    return web.Response(text=json.dumps({'result': result}), status=200, content_type='application/json')


async def handle_post(request):
    """ handles post """

    result = await Parser(**{'request': request}).comes_over_zero()

    return web.Response(text=json.dumps({'result': result}), status=200, content_type='application/json')

import datetime
import pytz
import random

from pprint import pprint

timezone = pytz.timezone('UTC')


class RequestGenerator():

    def __init__(self):
        self.request_body = {
            'station_config': {
                'station_id': 823,
                'longitude': 46.813,
                'latitude': -71.218,
                'timezone': 'MSK',
            },
            'data': self.__get_data(),
        }

    def __get_data(self):

        start = timezone.localize(
            datetime.datetime.now() - datetime.timedelta(minutes=30))

        result = {}
        positive = random.randint(-1, 0)

        for i in range(0, 300):
            if i < 299:
                t_air = random.uniform(0, 5)
                if positive == -1:
                    t_air = positive * random.uniform(0, 5)

            else:
                positive = random.randint(-1, 0)
                t_air = random.uniform(0, 5)
                if positive == -1:
                    t_air = positive * random.uniform(0, 5)
            result[datetime.datetime.strftime(
                start+datetime.timedelta(minutes=i), '%Y-%m-%d %H:%M %Z')] = {
                    't_air': t_air,
            }

        return result

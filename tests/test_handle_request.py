""" tests api """
import time
import random
import json
import unittest
import requests
import asyncio

from pprint import pprint

from request_generator import RequestGenerator


class TestHandleRequest(unittest.TestCase):
    """ tests handle requests view """

    def setUp(self):
        """ set up parameters """

        r_gen = RequestGenerator()

        self.params = r_gen.request_body
        self.url = 'http://54.39.22.136:2000'
        self.headers = {
            'Content-type': 'application/json',
        }

    def test_get_request(self):
        """ tests get request """

        r = requests.get(self.url, data=json.dumps(
            self.params))

        self.assertEqual(r.status_code, 200)

    def test_wrong_schema_get_request(self):
        """ tests wrong get request """

        r = requests.get(self.url, data=json.dumps({}))

        self.assertEqual(r.status_code, 400)

    def test_wrong_schema_post_request(self):
        """ tests wrong post request """

        data = {
            'station_config': {},
            'dataada': {
                '2016-12-30 10:00 UTC': {
                    't_air': -18.0,
                },
            },
        }

        r = requests.post(self.url, data=json.dumps(
            data), headers=self.headers)

        self.assertEqual(r.status_code, 400)

    def test_post_request(self):
        """ tests post request """

        r = requests.post(self.url, data=json.dumps(
            self.params), headers=self.headers)

        self.assertEqual(r.status_code, 200)

    def test_patch_request(self):
        """ tests patch request """

        r = requests.patch(self.url)

        self.assertEqual(r.status_code, 405)

    def test_put_request(self):
        """ tests patch request """

        r = requests.put(self.url)

        self.assertEqual(r.status_code, 405)


if __name__ == '__main__':
    unittest.main()

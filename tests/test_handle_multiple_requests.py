""" tests api """
import aiohttp
import asyncio
import random
import json
import unittest

from pprint import pprint

from request_generator import RequestGenerator


class TestHandleRequest(unittest.TestCase):
    """ tests handle requests view """

    def setUp(self):
        """ set up parameters """

        r_gen = RequestGenerator()

        # self.url = 'http://54.39.22.136:2000'
        self.url = 'http://0.0.0.0:2000'
        self.headers = {
            'Content-type': 'application/json',
        }

    @staticmethod
    async def fetch(session, url):
        async with session.post(url, json=RequestGenerator().request_body) as response:
            return await response.json()

    async def main(self):
        session = aiohttp.ClientSession()
        html = await self.fetch(session, self.url)
        pprint(html)
        await session.close()

    def test_multiple_posts_request(self):
        """ test multiple requests """

        loop = asyncio.get_event_loop()

        tasks = [self.main() for i in range(350)]

        loop.run_until_complete(asyncio.wait(tasks))
        loop.close()


if __name__ == '__main__':
    unittest.main()
